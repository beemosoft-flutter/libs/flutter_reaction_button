import 'package:flutter/material.dart';

import '../models/reaction.dart';
import '../utils/reactions_position.dart';
import 'reactions_box.dart';

class ReactionContainer<T> extends StatefulWidget {
  /// This triggers when reaction button value changed.
  final void Function(T?) onReactionChanged;

  /// Previous selected reaction widget
  final Reaction<T>? selectedReaction;

  final List<Reaction<T>?> reactions;

  /// Vertical position of the reactions box relative to the button [default = VerticalPosition.TOP]
  final VerticalPosition boxPosition;

  /// Horizontal position of the reactions box relative to the button [default = HorizontalPosition.START]
  final HorizontalPosition boxHorizontalPosition;

  /// Reactions box color [default = white]
  final Color boxColor;

  /// Reactions box elevation [default = 5]
  final double boxElevation;

  /// Reactions box radius [default = 50]
  final double boxRadius;

  /// Reactions box show/hide duration [default = 200 milliseconds]
  final Duration boxDuration;

  /// Reactions box padding [default = const EdgeInsets.all(0)]
  final EdgeInsets boxPadding;

  /// Scale ratio when item hovered [default = 0.3]
  final double itemScale;

  /// Scale duration while dragging [default = const Duration(milliseconds: 100)]
  final Duration? itemScaleDuration;

  final VoidCallback? onChildLongPressed;

  final Widget child;

  const ReactionContainer({
    Key? key,
    required this.onReactionChanged,
    required this.reactions,
    this.selectedReaction,
    this.boxPosition = VerticalPosition.top,
    this.boxHorizontalPosition = HorizontalPosition.start,
    this.boxColor = Colors.white,
    this.boxElevation = 5,
    this.boxRadius = 50,
    this.boxDuration = const Duration(milliseconds: 200),
    this.boxPadding = const EdgeInsets.all(0),
    this.itemScale = .3,
    this.itemScaleDuration,
    this.onChildLongPressed,
    required this.child,
  }) : super(key: key);

  @override
  State<ReactionContainer<T>> createState() => _ReactionContainerState<T>();
}

class _ReactionContainerState<T> extends State<ReactionContainer<T>> {
  OverlayEntry? _overlayEntry;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onLongPressStart: (details) {
        if (widget.onChildLongPressed != null) {
          widget.onChildLongPressed!();
        }
        _showReactionsBox(context, details.globalPosition);
      },
      child: widget.child,
    );
  }

  void _showReactionsBox(BuildContext context, Offset buttonOffset) async {
    final reactionButton = await Navigator.of(context).push(
      PageRouteBuilder(
        opaque: false,
        transitionDuration: const Duration(milliseconds: 200),
        pageBuilder: (_, __, ___) {
          return ReactionsBox(
            buttonOffset: buttonOffset,
            buttonSize: Size.zero,
            reactions: widget.reactions,
            verticalPosition: widget.boxPosition,
            horizontalPosition: widget.boxHorizontalPosition,
            color: widget.boxColor,
            elevation: widget.boxElevation,
            radius: widget.boxRadius,
            duration: widget.boxDuration,
            boxPadding: widget.boxPadding,
            itemScale: widget.itemScale,
            itemScaleDuration: widget.itemScaleDuration,
          );
        },
      ),
    );

    if (reactionButton != null) {
      widget.onReactionChanged.call(reactionButton.value);
    } else {
      widget.onReactionChanged.call(null);
    }
  }

  void _showReactionView(BuildContext context, Offset buttonOffset) {
    _overlayEntry = OverlayEntry(
      builder: (BuildContext context) {
        return GestureDetector(
          onTap: () {
            // Bat su kien nhung khong lam gi de ngan viec an ban phim
          },
          child: ReactionsBox(
            buttonOffset: buttonOffset,
            buttonSize: Size.zero,
            onReactionChanged: (reactionButton) {
              if (reactionButton != null) {
                widget.onReactionChanged.call(reactionButton.value);
                _overlayEntry?.remove();
              } else {
                widget.onReactionChanged.call(null);
                _overlayEntry?.remove();
              }
            },
            reactions: widget.reactions,
            verticalPosition: widget.boxPosition,
            horizontalPosition: widget.boxHorizontalPosition,
            color: widget.boxColor,
            elevation: widget.boxElevation,
            radius: widget.boxRadius,
            duration: widget.boxDuration,
            boxPadding: widget.boxPadding,
            itemScale: widget.itemScale,
            itemScaleDuration: widget.itemScaleDuration,
          ),
        );
      },
    );
    Overlay.of(context).insert(_overlayEntry!);
  }
}
